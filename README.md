## nginx upstream least time module
> Note: steps 1-3 may be skipped if you want to install application from deb package.

####  1. Installing prerequisites (if needed)
```shell
cd ~
sudo apt-get update
sudo apt-get install build-essential
sudo apt-get install openssl
sudo apt-get install libssl-dev
sudo apt-get install geoip-bin geoip-database
sudo apt-get install libgeoip-dev
sudo apt-get install libpcre3 libpcre3-dev
sudo apt-get install zlib1g-dev

```
####  2. Building nginx and module
```shell
wget 'http://nginx.org/download/nginx-1.15.11.tar.gz'
tar -xzvf nginx-1.15.11.tar.gz
cd nginx-1.15.11
sudo apt-get install patch
patch -p1 < ~/ngx_http_upstream_least_time-master/least_time_mod.patch
cp ~/ngx_http_upstream_least_time-master/scripted ./configure_script
chmod +x ./configure_script && ./configure_script
make
```
#### 3. Run compiled application
```shell
cd objs
./nginx -V
sudo ./nginx -p ./ -c ./nginx.conf
```
#### 4. Installing from deb package
```shell
wget 'https://pm.sip.az/projects/delivery-app/repository/ngx_http_upstream_least_time/revisions/master/entry/Debian/nginx-upstream_1.0.0_amd64.deb'
sudo dpkg -i nginx-upstream_1.0.0_amd64.deb
```

To check the installed application type in console 
`/etc/init.d/nginx_upstream status` or 
`systemctl status nginx_upstream.service`.
To view access log file type `tail -f /usr/lib/sip/logs/access.log`.

#### 5.Usage
To configure upstream servers edit nginx.conf file located in `/usr/lib/sip/config/nginx.conf` directory.

```shell
...
    upstream backend {
        least_time base=header mode=current;
        server 167.114.254.95:443 weight=2;
        server 167.114.224.247:443 weight=2;
        server 167.114.233.99:443 weight=1;
        server 54.36.114.142:443 weight=2;
    }
...
```
